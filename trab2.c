/*
Nome1: Henrique Teruo Eihara RA: 490016
Nome2:
*/

#include<string.h>
#include<stdio.h>
#include<stdlib.h>

typedef struct no {
  char info;
  struct no *esq, *dir;
} No;

typedef No * Arvore;

void preorder(No* p){
	if(p != NULL){
		printf("%c",p->info);
		preorder(p->esq);
		preorder(p->dir);	
	}
}

void postorder(No* p){
	if(p != NULL){
		postorder(p->esq);
		postorder(p->dir);	
		printf("%c",p->info);
	}
}

No *arv(char c, No* esq, No* dir){
  No *novo;

  novo = (No *)malloc(sizeof(No));
  novo->info = c;
  novo->esq = esq;
  novo->dir = dir;

  return novo;
}

Arvore monta_arv(char exp[], int *pos, int tam){
  Arvore raiz = NULL;
  Arvore esq = NULL;
  Arvore dir = NULL;
  Arvore aux = NULL;
  Arvore aux2 = NULL;

  while(exp[*pos] != '\0'){
    if((exp[*pos] >= 97)&&(exp[*pos]<=127)){
      if(esq == NULL)
        esq = arv(exp[*pos],NULL,NULL);
      else
        dir = arv(exp[*pos],NULL,NULL);
    }else{
      if((exp[*pos] >= 42)&&(exp[*pos] <= 47)){
        if(raiz == NULL)
          raiz = arv(exp[*pos],NULL,NULL);
        else{
          printf("entrei aqui\n");
          aux = monta_arv(exp,pos,tam);
        }
      }else{
        if(exp[*pos] == '('){
          // abertura de parenteses
          *pos+=1;
          if(esq == NULL)
            esq = monta_arv(exp,pos,tam);
          else
            dir = monta_arv(exp,pos,tam);
        }else{
          break;
        }
      }
    }

    *pos+=1;
  }
  raiz->esq = esq;
  raiz->dir = dir;

  if(aux != NULL){
    aux->dir = aux->esq;
    aux->esq = raiz;
    return aux;
  }

  return raiz;
}

int main(){
  char exp[12345];
  Arvore t;
  int n;

  fgets(exp, 12345, stdin);
  while (strcmp(exp, "0\n") != 0){
    n = 0;
    t = monta_arv(exp,&n, strlen(exp)-1); // strlen(exp) - 1 para ignorar o \n

    preorder(t);
    printf("\n");
    postorder(t);
    printf("\n");

    fgets(exp, 12345, stdin);

  }

  return 0;
}
